/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ZMDELL
 */
@Entity
@Table(name = "market_session")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarketSession.findAll", query = "SELECT m FROM MarketSession m"),
    @NamedQuery(name = "MarketSession.findById", query = "SELECT m FROM MarketSession m WHERE m.id = :id"),
    @NamedQuery(name = "MarketSession.findByIdMarket", query = "SELECT m FROM MarketSession m WHERE m.idMarket = :idMarket"),
    @NamedQuery(name = "MarketSession.findByDate", query = "SELECT m FROM MarketSession m WHERE m.date = :date"),
    @NamedQuery(name = "MarketSession.findBySession", query = "SELECT m FROM MarketSession m WHERE m.session = :session"),
    @NamedQuery(name = "MarketSession.findByStart", query = "SELECT m FROM MarketSession m WHERE m.start = :start"),
    @NamedQuery(name = "MarketSession.findByEnd", query = "SELECT m FROM MarketSession m WHERE m.end = :end")})
public class MarketSession implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "id_market")
    private String idMarket;
    @Basic(optional = false)
    @Column(name = "date")
    private long date;
    @Basic(optional = false)
    @Column(name = "session")
    private int session;
    @Basic(optional = false)
    @Column(name = "start")
    private long start;
    @Basic(optional = false)
    @Column(name = "end")
    private long end;

    public MarketSession() {
    }

    public MarketSession(String id) {
        this.id = id;
    }

    public MarketSession(String id, String idMarket, long date, int session, long start, long end) {
        this.id = id;
        this.idMarket = idMarket;
        this.date = date;
        this.session = session;
        this.start = start;
        this.end = end;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMarket() {
        return idMarket;
    }

    public void setIdMarket(String idMarket) {
        this.idMarket = idMarket;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarketSession)) {
            return false;
        }
        MarketSession other = (MarketSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ryan.dsw.entity.MarketSession[ id=" + id + " ]";
    }
    
}
