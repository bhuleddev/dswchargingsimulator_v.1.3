/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ZMDELL
 */
@Entity
@Table(name = "demander")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Demander.findAll", query = "SELECT d FROM Demander d"),
    @NamedQuery(name = "Demander.findById", query = "SELECT d FROM Demander d WHERE d.id = :id"),
    @NamedQuery(name = "Demander.findByIdLike", query = "SELECT s FROM Demander s WHERE s.id LIKE :id"),
    @NamedQuery(name = "Demander.findByPrice", query = "SELECT d FROM Demander d WHERE d.price = :price"),
    @NamedQuery(name = "Demander.findByQty", query = "SELECT d FROM Demander d WHERE d.qty = :qty")})
public class Demander implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "price")
    private String price;
    @Basic(optional = false)
    @Column(name = "qty")
    private String qty;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    public Demander() {
    }

    public Demander(String id) {
        this.id = id;
    }

    public Demander(String id, String price, String qty) {
        this.id = id;
        this.price = price;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Demander)) {
            return false;
        }
        Demander other = (Demander) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ryan.dsw.entity.Demander[ id=" + id + " ]";
    }
    
}
