/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ZMDELL
 */
@Entity
@Table(name = "supdem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Supdem.findAll", query = "SELECT s FROM Supdem s"),
    @NamedQuery(name = "Supdem.findById", query = "SELECT s FROM Supdem s WHERE s.id = :id"),
    @NamedQuery(name = "Supdem.findByIdLike", query = "SELECT s FROM Supdem s WHERE s.id LIKE :id"),
    @NamedQuery(name = "Supdem.findByKwh", query = "SELECT s FROM Supdem s WHERE s.kwh = :kwh"),
    @NamedQuery(name = "Supdem.findByPrice", query = "SELECT s FROM Supdem s WHERE s.price = :price"),
    @NamedQuery(name = "Supdem.findByType", query = "SELECT s FROM Supdem s WHERE s.type = :type")})
public class Supdem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "kwh")
    private float kwh;
    @Basic(optional = false)
    @Column(name = "price")
    private float price;
    @Basic(optional = false)
    @Column(name = "type")
    private String type;

    public Supdem() {
    }

    public Supdem(String id) {
        this.id = id;
    }

    public Supdem(String id, float kwh, float price, String type) {
        this.id = id;
        this.kwh = kwh;
        this.price = price;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getKwh() {
        return kwh;
    }

    public void setKwh(float kwh) {
        this.kwh = kwh;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Supdem)) {
            return false;
        }
        Supdem other = (Supdem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ryan.dsw.entity.Supdem[ id=" + id + " ]";
    }
    
}
