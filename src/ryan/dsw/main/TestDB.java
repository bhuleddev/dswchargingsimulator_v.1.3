/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.main;

import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import ryan.dsw.entity.Market;
import ryan.dsw.util.DBUtil;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import ryan.dsw.entity.Demander;
import ryan.dsw.entity.Supplier;
import ryan.dsw.util.SupplierComparator;

/**
 *
 * @author Bhuleddev
 */
public class TestDB {

    static EntityManager em;

//    public static void main(String args[]) {
//        int noID = 1;
//        int noLoc = 101;
//        Random randomGenerator = new Random();
//
//        DBUtil du = new DBUtil();
//        em.getTransaction().begin();
//
//        em.createNativeQuery("truncate table Market").executeUpdate();
//        Query q = em.createQuery("select m from Market m");
//        boolean createNewEntries = (q.getResultList().size() == 0);
//        int i = 0;
//        if (createNewEntries) {
//            while (i <= 10) {
//                int randomInt = randomGenerator.nextInt(20);
//                Market market = new Market();
//                int x = noID + randomInt;
//                market.setId("M-" + x);
//                x = noLoc + randomInt;
//                market.setLocation("UK-" + x);
//                q = em.createQuery("Select m from Market m where m.id=:arg1");
//                q.setParameter("arg1", market.getId());
//                if (q.getResultList().size() > 0) {
//                    System.out.println("ID Exists");
//                } else {
//                    em.persist(market);
//                    i++;
//                }
//            }
//            em.getTransaction().commit();
//            q = em.createQuery("select m from Market m");
//            List<Market> ms = q.getResultList();
//            for (Market market : ms) {
//                System.out.println("MARKET:"+market.getId()+"~Postcode:"+market.getLocation());
//            }
//            em.close();
//        }
//    }
    public static void main(String args[]) {
        HashMap<String, String> config = new HashMap<String, String>();
        config.put("dTotal", "10");
        config.put("dPriceMin", "0");
        config.put("dPriceMax", "30");
        config.put("dQtyMin", "0");
        config.put("dQtyMax", "50");
        config.put("sTotal", "6");
        config.put("sPriceMin", "0");
        config.put("sPriceMax", "5");
        config.put("sQtyMin", "0");
        config.put("sQtyMax", "5");

        DBUtil db = new DBUtil(config);
        db.clearTable("supplier");
        List<Supplier> dem = db.generateSupplier("xxx");
//        insertionSortDemander(dem);
        System.out.println("BEFORE");
        for (Supplier demander : dem) {
            System.out.println(demander.getId() + "--" + demander.getPrice() + "--" + demander.getQty());
        }
        Collections.sort(dem, new SupplierComparator());
        System.out.println("AFTER");
        for (Supplier demander : dem) {
            System.out.println(demander.getId() + "--" + demander.getPrice() + "--" + demander.getQty());
        }
    }

    public static void insertionSortDemander(List<Demander> obj) {
        int n = obj.size();
        for (int j = 1; j < n; j++) {
            Demander key = obj.get(j);
            int i = j;
            while (i > 0) {
                if (Float.parseFloat(obj.get(i - 1).getPrice()) > Float.parseFloat(key.getPrice())) {
                    System.out.println("1");
                    obj.set(i, obj.get(i - 1));
                } else if (Float.parseFloat(obj.get(i - 1).getQty()) > Float.parseFloat(key.getQty())) {
                    System.out.println("2");
                    obj.set(i, obj.get(i - 1));
                }
            }
            obj.set(i, key);
        }
        System.out.println("SIZE:" + obj.size());
        for (Demander demander : obj) {
            System.out.println(demander.getId() + "--" + demander.getPrice() + "--" + demander.getQty());
        }
    }
}
