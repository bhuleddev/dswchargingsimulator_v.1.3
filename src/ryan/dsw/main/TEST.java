package ryan.dsw.main;

import au.com.bytecode.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ryan Permana
 */
public class TEST {

    public static void main(String args[]) throws FileNotFoundException, IOException {
        CSVReader reader = new CSVReader(new FileReader("csv/supplier.csv"));
        List myEntries = reader.readAll();
        List<demander> ds = new ArrayList<demander>();
        for (int i = 1; i < myEntries.size(); i++) {
            String[] x = (String[]) myEntries.get(i);
            demander d = new demander();
            d.setId(x[0]);
            d.setName(x[1]);
            d.setPrice(x[2]);
            d.setQty(x[3]);
            d.setBudget(x[4]);
            ds.add(d);
        }

        for (demander d : ds) {
            System.out.println("ID:" + d.getId());
            System.out.println("Name:" + d.getName());
            System.out.println("Price:" + d.getPrice());
            System.out.println("QTY:" + d.getQty());
            System.out.println("Earnings:" + d.getBudget());
            System.out.println("=====================");
        }
    }
}

class demander {

    String id;
    String name;
    String price;
    String qty;
    String budget;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }
}