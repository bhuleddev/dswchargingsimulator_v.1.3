package ryan.dsw.main;

import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import ryan.dsw.entity.Demander;
import ryan.dsw.entity.Supplier;
import ryan.dsw.main.subFrame.chartsWindow;
import ryan.dsw.main.subFrame.demanderSorted;
import ryan.dsw.main.subFrame.supdemWindow;
import ryan.dsw.main.subFrame.supplierSorted;
import ryan.dsw.util.DBUtil;
import ryan.dsw.util.Param;
import ryan.dsw.util.SupplierComparator;

/**
 *
 * @author Ryan Permana
 */
public class SupDem extends javax.swing.JFrame {

    long date = 0l;
    String id = "";
    DefaultTableModel tbModelS, tbModelD;
    DBUtil db;
    List<ryan.dsw.entity.Supplier> sd, sdSort;
    List<ryan.dsw.entity.Demander> dd, ddSort;

    public SupDem() {
        initComponents();
    }

    public SupDem(String id, Long date, File csvDemand, File csvSupply) {
        try {
            initComponents();
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            this.date = date;
            this.id = id;
            sdLbl.setText("Market:" + id.split("~")[0] + " Session:" + id.split("~")[1]);
            String sess = Param.session[Integer.parseInt(id.split("~")[1])];
            sdDateLbl.setText(sess + ".00-" + sess + ".50");

            //Demander Put
            CSVReader readerD = null;
            if (csvDemand != null) {
                if (csvDemand.getName() != null) {
                    readerD = new CSVReader(new FileReader("csv/demander/" + csvDemand.getName()));
                }
            } else {
                readerD = new CSVReader(new FileReader("csv/demander/demander.csv"));
            }
            List myEntries = readerD.readAll();
            dd = new ArrayList<Demander>();
            for (int i = 1; i < myEntries.size(); i++) {
                String[] x = (String[]) myEntries.get(i);
                Demander d = new Demander();
                d.setId(x[0]);
                d.setName(x[1]);
                d.setPrice(x[2]);
                d.setQty(x[3]);
                dd.add(d);
            }
            db = new DBUtil();
            db.clearTable("demander");
            db.putDemanderBuck(dd);
            tbModelD = new DefaultTableModel(new Object[]{"ID", "Name", "Price", "Quantity", "Budget"}, 0);
            for (ryan.dsw.entity.Demander ddss : dd) {
                String[] data = new String[6];
                data[0] = ddss.getId();
                data[1] = ddss.getName();
                data[2] = ddss.getPrice();
                data[3] = ddss.getQty();
                data[4] = String.valueOf(Float.parseFloat(ddss.getPrice()) * Float.parseFloat(ddss.getQty()));
                tbModelD.addRow(data);
            }
            dTbl.setModel(tbModelD);
                    
            //Supplier Put
            CSVReader readerS = null;
            if (csvSupply != null) {
                if (csvSupply.getName() != null) {
                    readerS = new CSVReader(new FileReader("csv/supplier/" + csvSupply.getName()));
                }
            } else {
                readerS = new CSVReader(new FileReader("csv/supplier/supplier.csv"));
            }
            List myEntriesS = readerS.readAll();
            sd = new ArrayList<Supplier>();
            for (int i = 1; i < myEntriesS.size(); i++) {
                String[] x = (String[]) myEntriesS.get(i);
                Supplier d = new Supplier();
                d.setId(x[0]);
                d.setName(x[1]);
                d.setPrice(x[2]);
                d.setQty(x[3]);
                sd.add(d);
            }
            db = new DBUtil();
            db.clearTable("supplier");
            db.putSupplierBuck(sd);
            tbModelS = new DefaultTableModel(new Object[]{"ID", "Name", "Price", "Quantity", "Earning"}, 0);
            for (ryan.dsw.entity.Supplier ddss : sd) {
                String[] data = new String[6];
                data[0] = ddss.getId();
                data[1] = ddss.getName();
                data[2] = ddss.getPrice();
                data[3] = ddss.getQty();
                data[4] = String.valueOf(Float.parseFloat(ddss.getPrice()) * Float.parseFloat(ddss.getQty()));
                tbModelS.addRow(data);
            }
            sTbl.setModel(tbModelS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public SupDem(String id, Long date, HashMap<String, String> config) {
        try {
            initComponents();
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            this.date = date;
            this.id = id;

            db = new DBUtil(config);
            sdLbl.setText("Market:" + id.split("~")[0] + " Session:" + id.split("~")[1]);
            String sess = Param.session[Integer.parseInt(id.split("~")[1])];
            sdDateLbl.setText(sess + ".00-" + sess + ".50");

            db.clearTable("supplier");
            tbModelS = new DefaultTableModel(new Object[]{"ID", "Price", "Quantity"}, 0);
            sd = db.generateSupplier(id);
            for (ryan.dsw.entity.Supplier sdss : sd) {
                String[] data = new String[6];
                data[0] = sdss.getId();
                data[1] = sdss.getPrice();
                data[2] = sdss.getQty();
                tbModelS.addRow(data);
            }
            sTbl.setModel(tbModelS);

            db.clearTable("demander");
            tbModelD = new DefaultTableModel(new Object[]{"ID", "Price", "Quantity"}, 0);
            dd = db.generateDemander(id);
            for (ryan.dsw.entity.Demander ddss : dd) {
                String[] data = new String[6];
                data[0] = ddss.getId();
                data[1] = ddss.getPrice();
                data[2] = ddss.getQty();
                tbModelD.addRow(data);
            }
            dTbl.setModel(tbModelD);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        dSortBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        dTbl = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        sSortBtn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        sTbl = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        sdLbl = new javax.swing.JLabel();
        sdDateLbl = new javax.swing.JLabel();
        sSortBtn1 = new javax.swing.JButton();
        btn_table = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        dSortBtn.setText("SORT!");
        dSortBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dSortBtnActionPerformed(evt);
            }
        });

        dTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(dTbl);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dSortBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 703, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dSortBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Demander", jPanel1);

        sSortBtn.setText("SORT!");
        sSortBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sSortBtnActionPerformed(evt);
            }
        });

        sTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(sTbl);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sSortBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 703, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(sSortBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Supplier", jPanel2);

        sdLbl.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        sdLbl.setText("jLabel1");

        sdDateLbl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        sdDateLbl.setText("jLabel1");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(sdDateLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sdLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE))
                    .addGap(9, 9, 9)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(sdLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(sdDateLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        sSortBtn1.setText("GRAPH");
        sSortBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sSortBtn1ActionPerformed(evt);
            }
        });

        btn_table.setText("TABLE");
        btn_table.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(sSortBtn1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btn_table, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sSortBtn1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(btn_table)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sSortBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sSortBtnActionPerformed
        sdSort = sd;
        Collections.sort(sdSort, new SupplierComparator());
//        DefaultTableModel tbModelSsort = new DefaultTableModel(new Object[]{"ID", "Price", "Quantity","Cum_Qty"}, 0);
//        Float cQty = 0f;
//        for (ryan.dsw.entity.Supplier sdss : sdSort) {
//            String[] data = new String[6];
//            data[0] = sdss.getId();
//            data[1] = sdss.getPrice();
//            data[2] = sdss.getQty();
//            cQty = cQty+Float.parseFloat(data[2]);
//            data[3] = String.valueOf(cQty);
//            tbModelSsort.addRow(data);
//        }
//        sTbl.setModel(tbModelSsort);

        if (supplierSorted.stats_ == true) {
            supplierSorted.stats_ = false;
            new supplierSorted(id, date, sdSort).setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "This window is already open");
        }


    }//GEN-LAST:event_sSortBtnActionPerformed

    private void dSortBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dSortBtnActionPerformed
        ddSort = dd;
//        Collections.sort(ddSort, new DemanderComparator());
//        DefaultTableModel tbModelDsort = new DefaultTableModel(new Object[]{"ID", "Price", "Quantity","Cum_Qty"}, 0);
//        Float cQty = 0f;
//        for (ryan.dsw.entity.Demander sdss : ddSort) {
//            String[] data = new String[6];
//            data[0] = sdss.getId();
//            data[1] = sdss.getPrice();
//            data[2] = sdss.getQty();
//            cQty = cQty+Float.parseFloat(data[2]);
//            data[3] = String.valueOf(cQty);
//            tbModelDsort.addRow(data);
//        }
//        dTbl.setModel(tbModelDsort);
        if (demanderSorted.stats_ == true) {
            demanderSorted.stats_ = false;
            new demanderSorted(id, date, ddSort).setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "This window is already open");
        }
    }//GEN-LAST:event_dSortBtnActionPerformed

    private void sSortBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sSortBtn1ActionPerformed
        if (chartsWindow.statsDouble_ == true) {
            chartsWindow.statsDouble_ = false;
            XYDataset data = createDataset(sd, dd);
            new chartsWindow(data, "Demander Supplier Chart").setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "This window is already open");
        }

    }//GEN-LAST:event_sSortBtn1ActionPerformed

    private void btn_tableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tableActionPerformed
         ddSort = dd;
         sdSort = sd;
        if (supdemWindow.stats_ == true) {
            supdemWindow.stats_ = false;
            new supdemWindow(id, date, ddSort,sdSort).setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "This window is already open");
        }
    }//GEN-LAST:event_btn_tableActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SupDem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SupDem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SupDem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SupDem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SupDem().setVisible(true);
            }
        });
    }

    private static XYDataset createDataset(List<Supplier> dataS, List<Demander> dataD) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries seriesS = new XYSeries("Supplier");
        XYSeries seriesD = new XYSeries("Demander");
        for (Supplier demander : dataS) {
            seriesS.add(Float.parseFloat(demander.getPrice()), Float.parseFloat(demander.getQty()));
        }
        for (Demander demander : dataD) {
            seriesD.add(Float.parseFloat(demander.getPrice()), Float.parseFloat(demander.getQty()));
        }
        dataset.addSeries(seriesS);
        dataset.addSeries(seriesD);
        return dataset;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_table;
    private javax.swing.JButton dSortBtn;
    private javax.swing.JTable dTbl;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton sSortBtn;
    private javax.swing.JButton sSortBtn1;
    private javax.swing.JTable sTbl;
    private javax.swing.JLabel sdDateLbl;
    private javax.swing.JLabel sdLbl;
    // End of variables declaration//GEN-END:variables
}
