/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import ryan.dsw.entity.Market;
import ryan.dsw.entity.MarketSession;
import ryan.dsw.entity.Supdem;
import ryan.dsw.entity.Supplier;
import ryan.dsw.entity.Demander;

/**
 *
 * @author ZMDELL
 */
public class DBUtil {

    private static final String PERSISTENCE_UNIT_NAME = "DSWChargingSimulatorPU";
    private EntityManagerFactory factory;
    private EntityManager em;
    private int dTotal, sTotal = 0;
    private int dPriceMax, dPriceMin, dQtyMax, dQtyMin, sPriceMax, sPriceMin, sQtyMax, sQtyMin = 0;

    public DBUtil() {
//        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        factory = PersistenceUtilities.getEntityManagerFactory();
    }

    public DBUtil(HashMap<String, String> config) {
//        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        factory = PersistenceUtilities.getEntityManagerFactory();
        if (config != null) {
            if (!config.get("dTotal").equals("")) {
                dTotal = Integer.parseInt(config.get("dTotal"));
            } else {
                dTotal = 20;
            }
            if (!config.get("dPriceMax").equals("")) {
                dPriceMax = Integer.parseInt(config.get("dPriceMax"));
            } else {
                dPriceMax = 100;
            }
            if (!config.get("dPriceMin").equals("")) {
                dPriceMin = Integer.parseInt(config.get("dPriceMin"));
            } else {
                dPriceMin = 0;
            }
            if (!config.get("dQtyMax").equals("")) {
                dQtyMax = Integer.parseInt(config.get("dQtyMax"));
            } else {
                dQtyMax = 100;
            }
            if (!config.get("dQtyMin").equals("")) {
                dQtyMin = Integer.parseInt(config.get("dQtyMin"));
            } else {
                dQtyMin = 0;
            }

            if (!config.get("sTotal").equals("")) {
                sTotal = Integer.parseInt(config.get("sTotal"));
            } else {
                sTotal = 20;
            }
            if (!config.get("sPriceMax").equals("")) {
                sPriceMax = Integer.parseInt(config.get("sPriceMax"));
            } else {
                sPriceMax = 100;
            }
            if (!config.get("sPriceMin").equals("")) {
                sPriceMin = Integer.parseInt(config.get("sPriceMin"));
            } else {
                sPriceMin = 0;
            }
            if (!config.get("sQtyMax").equals("")) {
                sQtyMax = Integer.parseInt(config.get("sQtyMax"));
            } else {
                sQtyMax = 100;
            }
            if (!config.get("sQtyMin").equals("")) {
                sQtyMin = Integer.parseInt(config.get("sQtyMin"));
            } else {
                sQtyMin = 0;
            }
        }
    }

    public EntityManagerFactory getFactory() {
        return factory;
    }

    public List<Market> generateMarket() {
        List<Market> res = new ArrayList<Market>();
        Random randomGenerator = new Random();
        int noID = 1;
        int noLoc = 101;

        em = factory.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createQuery("select m from Market m");
        System.out.println("ALL1:" + q.getResultList().size());
        boolean createNewEntries = (q.getResultList().size() == 0);
        int i = 1;
        int x = 0;
        if (createNewEntries) {
            while (i <= 10) {
                int randomInt = randomGenerator.nextInt(100);
                Market market = new Market();
                x = noID + randomInt;
                market.setId("M-" + x);
                x = noLoc + randomInt;
                market.setLocation("UK-" + x);
//                q = em.createQuery("Select m from Market m where m.id=:arg1");
//                q.setParameter("arg1", market.getId());
                System.out.println("ALL:" + em.createNamedQuery("Market.findAll").getResultList().size());
                Query cek = em.createNamedQuery("Market.findById")
                        .setParameter("id", market.getId());
                System.out.println("ID:" + market.getId() + "~~" + "Location:" + market.getLocation());
                System.out.println("SIZE:" + cek.getResultList());
                if (cek.getResultList().size() > 0) {
                    System.out.println("ID Exists");
                } else {
                    em.persist(market);
                    i++;
                }
            }
            q = em.createQuery("select m from Market m");
            List<Market> ms = q.getResultList();
            for (Market market : ms) {
                res.add(market);
            }
            em.getTransaction().commit();
//            em.close();
        } else {
            System.out.println("DATA EXISTS, Need Truncate");
        }
        return res;
    }

    public List<Market> getMarketDataAll() {
        List<Market> res = new ArrayList<Market>();
        try {
            em = factory.createEntityManager();
            em.getTransaction().begin();
            Query q = em.createQuery("select m from Market m");
            res = q.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return res;
    }

    public void clearTable(String namaTable) {
        try {
            em = factory.createEntityManager();
            em.getTransaction().begin();
            System.out.println("RESULTREMOVE:" + em.createNativeQuery("truncate table " + namaTable).executeUpdate());
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.close();
        }
    }

    public List<MarketSession> generateMarketSession(Long date) throws ParseException {
        List<MarketSession> res = new ArrayList<MarketSession>();
        Random randomGenerator = new Random();

        em = factory.createEntityManager();
        em.getTransaction().begin();
        int i = 1;
        int x = 0;
        Query q = em.createNamedQuery("Market.findAll");
        List<Market> markets = q.getResultList();
        q = em.createNamedQuery("MarketSession.findAll");

        if (q.getResultList().size() == 0) {
            clearTable("market");
            generateMarket();

            em = factory.createEntityManager();
            em.getTransaction().begin();
            q = em.createNamedQuery("Market.findAll");
            markets = q.getResultList();
            while (i <= 30) {
                int randomInt = randomGenerator.nextInt(10);
                int randomInt2 = randomGenerator.nextInt(16) + 1;
                MarketSession ms = new MarketSession();
                System.out.println("DATE:" + date);
                ms.setDate(date);
                System.out.println(randomInt);
                String idmarket = markets.get(randomInt).getId();
                ms.setIdMarket(idmarket);
                int session = randomInt2;
                String dateStr = new java.text.SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date(date * 1000));
                ms.setSession(session);
                Long startUnix = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm").parse(dateStr + " " + Param.session[session] + ":00").getTime() / 1000;
                ms.setStart(startUnix);
                Long endUnix = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm").parse(dateStr + " " + Param.session[session] + ":50").getTime() / 1000;
                ms.setEnd(endUnix);
                ms.setId(idmarket + "~" + session);
                Query cek = em.createNamedQuery("MarketSession.findById")
                        .setParameter("id", ms.getId());
                System.out.println("ID:" + ms.getId());
                System.out.println("SIZE:" + cek.getResultList());
                if (cek.getResultList().size() > 0) {
                    System.out.println("ID Exists");
                } else {
                    em.persist(ms);
                    i++;
                }
            }
            q = em.createNamedQuery("MarketSession.findAll");
            List<MarketSession> ms = q.getResultList();
            for (MarketSession market : ms) {
                res.add(market);
            }
            em.getTransaction().commit();
            em.close();
        } else {
            q = em.createNamedQuery("MarketSession.findAll");
            List<MarketSession> ms = q.getResultList();
            for (MarketSession market : ms) {
                res.add(market);
            }
            em.getTransaction().commit();
            em.close();
            System.out.println("DATA EXISTS, Need Truncate");
        }
        return res;
    }

    public void putSupplierBuck(List<Supplier> supList) {
        System.out.println("MASUK SUPPLIER");
        em = factory.createEntityManager();
        em.getTransaction().begin();
        for (Supplier s : supList) {
            Query cek = em.createNamedQuery("Supplier.findById")
                    .setParameter("id", s.getId());
            if (cek.getResultList().size() > 0) {
                System.out.println("ID Exists");
            } else {
                em.persist(s);
            }
        }
        em.getTransaction().commit();
        em.close();
    }

    public List<Supplier> getSupplierAll() {
        List<Supplier> res = new ArrayList<Supplier>();
        Query q = em.createNamedQuery("Supplier.findAll");
        List<Supplier> ms = q.getResultList();
        for (Supplier sd : ms) {
            res.add(sd);
        }
        em.close();
        return res;
    }

    public void putDemanderBuck(List<Demander> demList) {
        em = factory.createEntityManager();
        em.getTransaction().begin();
        for (Demander s : demList) {
            Query cek = em.createNamedQuery("Demander.findById")
                    .setParameter("id", s.getId());
            if (cek.getResultList().size() > 0) {
                System.out.println("ID Exists");
            } else {
                em.persist(s);
            }
        }
        em.getTransaction().commit();
        em.close();
    }

    public List<Demander> getDemanderAll() {
        List<Demander> res = new ArrayList<Demander>();
        Query q = em.createNamedQuery("Demander.findAll");
        List<Demander> ms = q.getResultList();
        for (Demander sd : ms) {
            res.add(sd);
        }
        em.close();
        return res;
    }
    
    
    public List<Supplier> generateSupplier(String id) {
        List<Supplier> res = new ArrayList<Supplier>();
        Random randomGenerator = new Random();
        int i = 0;

        em = factory.createEntityManager();
        em.getTransaction().begin();

        Query q = em.createNamedQuery("Supplier.findByIdLike").setParameter("id", "%" + id + "%");
        if (q.getResultList().size() == 0) {
            while (i < sTotal) {
                int randomInt = randomGenerator.nextInt(sTotal + 100);
                Supplier sup = new Supplier();
                sup.setId(id + "~" + randomInt);
                float randPrice = ((randomGenerator.nextInt(sPriceMax - sPriceMin + 1) + sPriceMin) + randomGenerator.nextFloat());
                float randQty = ((randomGenerator.nextInt(sQtyMax - sQtyMin + 1) + sQtyMin) + randomGenerator.nextFloat());
                sup.setPrice(String.valueOf(randPrice));
                sup.setQty(String.valueOf(randQty));
                Query cek = em.createNamedQuery("Supplier.findById")
                        .setParameter("id", sup.getId());
                System.out.println("ID:" + sup.getId());
                System.out.println("SIZE:" + cek.getResultList());
                if (cek.getResultList().size() > 0) {
                    System.out.println("ID Exists");
                } else {
                    em.persist(sup);
                    i++;
                }
            }
            em.getTransaction().commit();
            q = em.createNamedQuery("Supplier.findByIdLike").setParameter("id", "%" + id + "%");
            List<Supplier> ms = q.getResultList();
            for (Supplier sd : ms) {
                res.add(sd);
            }
            em.close();
        } else {
            q = em.createNamedQuery("Supplier.findByIdLike").setParameter("id", "%" + id + "%");
            List<Supplier> ms = q.getResultList();
            for (Supplier sd : ms) {
                res.add(sd);
            }
            em.close();
            System.out.println("DATA EXISTS, Need Truncate");
        }
        return res;
    }

    public List<Demander> generateDemander(String id) {
        List<Demander> res = new ArrayList<Demander>();
        Random randomGenerator = new Random();
        int i = 0;

        em = factory.createEntityManager();
        em.getTransaction().begin();

        Query q = em.createNamedQuery("Demander.findByIdLike").setParameter("id", "%" + id + "%");
        if (q.getResultList().size() == 0) {
            while (i < dTotal) {
                int randomInt = randomGenerator.nextInt(dTotal + 100);
                Demander sup = new Demander();
                sup.setId(id + "~" + randomInt);
                float randPrice = ((randomGenerator.nextInt(dPriceMax - dPriceMin + 1) + dPriceMin) + randomGenerator.nextFloat());
                float randQty = ((randomGenerator.nextInt(dQtyMax - dQtyMin + 1) + dQtyMin) + randomGenerator.nextFloat());
                sup.setPrice(String.valueOf(randPrice));
                sup.setQty(String.valueOf(randQty));
                Query cek = em.createNamedQuery("Demander.findById")
                        .setParameter("id", sup.getId());
                System.out.println("ID:" + sup.getId());
                System.out.println("SIZE:" + cek.getResultList());
                if (cek.getResultList().size() > 0) {
                    System.out.println("ID Exists");
                } else {
                    em.persist(sup);
                    i++;
                }
            }
            em.getTransaction().commit();
            q = em.createNamedQuery("Demander.findByIdLike").setParameter("id", "%" + id + "%");
            List<Demander> ms = q.getResultList();
            for (Demander sd : ms) {
                res.add(sd);
            }
            em.close();
        } else {
            q = em.createNamedQuery("Demander.findByIdLike").setParameter("id", "%" + id + "%");
            List<Demander> ms = q.getResultList();
            for (Demander sd : ms) {
                res.add(sd);
            }
            em.close();
            System.out.println("DATA EXISTS, Need Truncate");
        }
        return res;
    }
}
