/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ryan.dsw.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author przetyczce
 */
public class PersistenceUtilities {
    private static EntityManagerFactory entityManagerFactory;

    static {
        Properties prop = new Properties();
        try {
            FileInputStream in = new FileInputStream("config/persistence.properties");
            prop.load(in);
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	entityManagerFactory = Persistence.createEntityManagerFactory("DSWChargingSimulatorPU",prop);
    }

    public static EntityManagerFactory getEntityManagerFactory() {
	return entityManagerFactory;
    }

    public static void StopEntityManager() {
        entityManagerFactory.getCache().evictAll();
        entityManagerFactory.close();
    }
}
