/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Ryan Permana
 */
public class Util {
    public static int copyFile(String file, String target){
        int res = 0;
        InputStream inStream = null;
	OutputStream outStream = null;
    	try{
    	    File afile =new File(file);
    	    File bfile =new File(target);
    	    inStream = new FileInputStream(afile);
    	    outStream = new FileOutputStream(bfile);
    	    byte[] buffer = new byte[1024];
    	    int length;
    	    //copy the file content in bytes 
    	    while ((length = inStream.read(buffer)) > 0){
    	    	outStream.write(buffer, 0, length);
    	    }
    	    inStream.close();
    	    outStream.close();
    	    System.out.println("File is copied successful!");
            res = 1;
    	}catch(IOException e){
    	    e.printStackTrace();
    	}
        return res;
    }
}
