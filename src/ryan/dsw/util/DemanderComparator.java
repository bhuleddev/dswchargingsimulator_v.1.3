/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.util;

import java.util.Comparator;
import ryan.dsw.entity.Demander;

/**
 *
 * @author Ryan Permana
 */
public class DemanderComparator implements Comparator<Demander> {

    public int compare(Demander p1, Demander p2)
    {
        int res = 0;
        int priceResult = Float.compare(Float.parseFloat(p1.getPrice()),Float.parseFloat(p2.getPrice()));
        int qtyResult = Float.compare(Float.parseFloat(p1.getQty()),Float.parseFloat(p2.getQty()));
        
        if (priceResult != 0)
        {
//            System.out.println("PRICE");
            res= priceResult;
        }else if (qtyResult != 0)
        {
//            System.out.println("QTY");QT
            res= qtyResult;
        }
//        System.out.println("RES:"+res);
        return res;
    }
}
