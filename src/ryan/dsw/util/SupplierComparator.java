/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ryan.dsw.util;

import java.util.Comparator;
import ryan.dsw.entity.Supplier;

/**
 *
 * @author Ryan Permana
 */
public class SupplierComparator implements Comparator<Supplier> {

    public int compare(Supplier p1, Supplier p2)
    {
        int res = 0;
        int priceResult = Float.compare(Float.parseFloat(p1.getPrice()),Float.parseFloat(p2.getPrice()));
        int qtyResult = Float.compare(Float.parseFloat(p1.getQty()),Float.parseFloat(p2.getQty()));
        
        if (priceResult != 0)
        {
//            System.out.println("PRICE");
            res= priceResult;
        }else if (qtyResult != 0)
        {
//            System.out.println("QTY");
            res= qtyResult;
        }
//        System.out.println("RES:"+res);
        return res;
    }
}
